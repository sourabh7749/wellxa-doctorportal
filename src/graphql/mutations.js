/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createPatientProfile = /* GraphQL */ `
  mutation CreatePatientProfile(
    $input: CreatePatientProfileInput!
    $condition: ModelPatientProfileConditionInput
  ) {
    createPatientProfile(input: $input, condition: $condition) {
      id
      firstName
      lastName
      gender
      homephoneNumber
      sma
      cellPhone
      language
      location
      preferredCommunication
      profile
      userId
      medicalHistoryconsent
      createdAt
      updatedAt
    }
  }
`;
export const updatePatientProfile = /* GraphQL */ `
  mutation UpdatePatientProfile(
    $input: UpdatePatientProfileInput!
    $condition: ModelPatientProfileConditionInput
  ) {
    updatePatientProfile(input: $input, condition: $condition) {
      id
      firstName
      lastName
      gender
      homephoneNumber
      sma
      cellPhone
      language
      location
      preferredCommunication
      profile
      userId
      medicalHistoryconsent
      createdAt
      updatedAt
    }
  }
`;
export const deletePatientProfile = /* GraphQL */ `
  mutation DeletePatientProfile(
    $input: DeletePatientProfileInput!
    $condition: ModelPatientProfileConditionInput
  ) {
    deletePatientProfile(input: $input, condition: $condition) {
      id
      firstName
      lastName
      gender
      homephoneNumber
      sma
      cellPhone
      language
      location
      preferredCommunication
      profile
      userId
      medicalHistoryconsent
      createdAt
      updatedAt
    }
  }
`;
export const createDoctorsProfile = /* GraphQL */ `
  mutation CreateDoctorsProfile(
    $input: CreateDoctorsProfileInput!
    $condition: ModeldoctorsProfileConditionInput
  ) {
    createDoctorsProfile(input: $input, condition: $condition) {
      id
      firstName
      lastName
      speciality
      about
      inPerson
      inVideo
      title
      timeZone
      officePhone
      officeFax
      cellPhone
      practiceLogo
      preferedLanguage
      userId
      createdAt
      updatedAt
    }
  }
`;
export const updateDoctorsProfile = /* GraphQL */ `
  mutation UpdateDoctorsProfile(
    $input: UpdateDoctorsProfileInput!
    $condition: ModeldoctorsProfileConditionInput
  ) {
    updateDoctorsProfile(input: $input, condition: $condition) {
      id
      firstName
      lastName
      speciality
      about
      inPerson
      inVideo
      title
      timeZone
      officePhone
      officeFax
      cellPhone
      practiceLogo
      preferedLanguage
      userId
      createdAt
      updatedAt
    }
  }
`;
export const deleteDoctorsProfile = /* GraphQL */ `
  mutation DeleteDoctorsProfile(
    $input: DeleteDoctorsProfileInput!
    $condition: ModeldoctorsProfileConditionInput
  ) {
    deleteDoctorsProfile(input: $input, condition: $condition) {
      id
      firstName
      lastName
      speciality
      about
      inPerson
      inVideo
      title
      timeZone
      officePhone
      officeFax
      cellPhone
      practiceLogo
      preferedLanguage
      userId
      createdAt
      updatedAt
    }
  }
`;
export const createDoctorsCredentials = /* GraphQL */ `
  mutation CreateDoctorsCredentials(
    $input: CreateDoctorsCredentialsInput!
    $condition: ModeldoctorsCredentialsConditionInput
  ) {
    createDoctorsCredentials(input: $input, condition: $condition) {
      id
      stateLicense
      deaNumber
      physicianDob
      prescriberName
      userId
      createdAt
      updatedAt
    }
  }
`;
export const updateDoctorsCredentials = /* GraphQL */ `
  mutation UpdateDoctorsCredentials(
    $input: UpdateDoctorsCredentialsInput!
    $condition: ModeldoctorsCredentialsConditionInput
  ) {
    updateDoctorsCredentials(input: $input, condition: $condition) {
      id
      stateLicense
      deaNumber
      physicianDob
      prescriberName
      userId
      createdAt
      updatedAt
    }
  }
`;
export const deleteDoctorsCredentials = /* GraphQL */ `
  mutation DeleteDoctorsCredentials(
    $input: DeleteDoctorsCredentialsInput!
    $condition: ModeldoctorsCredentialsConditionInput
  ) {
    deleteDoctorsCredentials(input: $input, condition: $condition) {
      id
      stateLicense
      deaNumber
      physicianDob
      prescriberName
      userId
      createdAt
      updatedAt
    }
  }
`;
export const createDoctorsPractice = /* GraphQL */ `
  mutation CreateDoctorsPractice(
    $input: CreateDoctorsPracticeInput!
    $condition: ModeldoctorsPracticeConditionInput
  ) {
    createDoctorsPractice(input: $input, condition: $condition) {
      id
      AddressLine1
      AddressLine2
      city
      state
      zip
      aboutPractice
      lattitude
      longitude
      userId
      createdAt
      updatedAt
    }
  }
`;
export const updateDoctorsPractice = /* GraphQL */ `
  mutation UpdateDoctorsPractice(
    $input: UpdateDoctorsPracticeInput!
    $condition: ModeldoctorsPracticeConditionInput
  ) {
    updateDoctorsPractice(input: $input, condition: $condition) {
      id
      AddressLine1
      AddressLine2
      city
      state
      zip
      aboutPractice
      lattitude
      longitude
      userId
      createdAt
      updatedAt
    }
  }
`;
export const deleteDoctorsPractice = /* GraphQL */ `
  mutation DeleteDoctorsPractice(
    $input: DeleteDoctorsPracticeInput!
    $condition: ModeldoctorsPracticeConditionInput
  ) {
    deleteDoctorsPractice(input: $input, condition: $condition) {
      id
      AddressLine1
      AddressLine2
      city
      state
      zip
      aboutPractice
      lattitude
      longitude
      userId
      createdAt
      updatedAt
    }
  }
`;
export const createDoctorsEducation = /* GraphQL */ `
  mutation CreateDoctorsEducation(
    $input: CreateDoctorsEducationInput!
    $condition: ModeldoctorsEducationConditionInput
  ) {
    createDoctorsEducation(input: $input, condition: $condition) {
      id
      education
      userId
      createdAt
      updatedAt
    }
  }
`;
export const updateDoctorsEducation = /* GraphQL */ `
  mutation UpdateDoctorsEducation(
    $input: UpdateDoctorsEducationInput!
    $condition: ModeldoctorsEducationConditionInput
  ) {
    updateDoctorsEducation(input: $input, condition: $condition) {
      id
      education
      userId
      createdAt
      updatedAt
    }
  }
`;
export const deleteDoctorsEducation = /* GraphQL */ `
  mutation DeleteDoctorsEducation(
    $input: DeleteDoctorsEducationInput!
    $condition: ModeldoctorsEducationConditionInput
  ) {
    deleteDoctorsEducation(input: $input, condition: $condition) {
      id
      education
      userId
      createdAt
      updatedAt
    }
  }
`;
export const createDoctorsAcceptedInsurance = /* GraphQL */ `
  mutation CreateDoctorsAcceptedInsurance(
    $input: CreateDoctorsAcceptedInsuranceInput!
    $condition: ModeldoctorsAcceptedInsuranceConditionInput
  ) {
    createDoctorsAcceptedInsurance(input: $input, condition: $condition) {
      id
      insurance
      userId
      createdAt
      updatedAt
    }
  }
`;
export const updateDoctorsAcceptedInsurance = /* GraphQL */ `
  mutation UpdateDoctorsAcceptedInsurance(
    $input: UpdateDoctorsAcceptedInsuranceInput!
    $condition: ModeldoctorsAcceptedInsuranceConditionInput
  ) {
    updateDoctorsAcceptedInsurance(input: $input, condition: $condition) {
      id
      insurance
      userId
      createdAt
      updatedAt
    }
  }
`;
export const deleteDoctorsAcceptedInsurance = /* GraphQL */ `
  mutation DeleteDoctorsAcceptedInsurance(
    $input: DeleteDoctorsAcceptedInsuranceInput!
    $condition: ModeldoctorsAcceptedInsuranceConditionInput
  ) {
    deleteDoctorsAcceptedInsurance(input: $input, condition: $condition) {
      id
      insurance
      userId
      createdAt
      updatedAt
    }
  }
`;
export const createDoctorsAffiliation = /* GraphQL */ `
  mutation CreateDoctorsAffiliation(
    $input: CreateDoctorsAffiliationInput!
    $condition: ModeldoctorsAffiliationConditionInput
  ) {
    createDoctorsAffiliation(input: $input, condition: $condition) {
      id
      affiliation
      userId
      createdAt
      updatedAt
    }
  }
`;
export const updateDoctorsAffiliation = /* GraphQL */ `
  mutation UpdateDoctorsAffiliation(
    $input: UpdateDoctorsAffiliationInput!
    $condition: ModeldoctorsAffiliationConditionInput
  ) {
    updateDoctorsAffiliation(input: $input, condition: $condition) {
      id
      affiliation
      userId
      createdAt
      updatedAt
    }
  }
`;
export const deleteDoctorsAffiliation = /* GraphQL */ `
  mutation DeleteDoctorsAffiliation(
    $input: DeleteDoctorsAffiliationInput!
    $condition: ModeldoctorsAffiliationConditionInput
  ) {
    deleteDoctorsAffiliation(input: $input, condition: $condition) {
      id
      affiliation
      userId
      createdAt
      updatedAt
    }
  }
`;
export const createDoctorsSetting = /* GraphQL */ `
  mutation CreateDoctorsSetting(
    $input: CreateDoctorsSettingInput!
    $condition: ModeldoctorsSettingConditionInput
  ) {
    createDoctorsSetting(input: $input, condition: $condition) {
      id
      roomName
      roomPasscode
      userName
      emailAlert
      smsAlert
      desktopAlert
      expDate
      cvv
      userId
      createdAt
      updatedAt
    }
  }
`;
export const updateDoctorsSetting = /* GraphQL */ `
  mutation UpdateDoctorsSetting(
    $input: UpdateDoctorsSettingInput!
    $condition: ModeldoctorsSettingConditionInput
  ) {
    updateDoctorsSetting(input: $input, condition: $condition) {
      id
      roomName
      roomPasscode
      userName
      emailAlert
      smsAlert
      desktopAlert
      expDate
      cvv
      userId
      createdAt
      updatedAt
    }
  }
`;
export const deleteDoctorsSetting = /* GraphQL */ `
  mutation DeleteDoctorsSetting(
    $input: DeleteDoctorsSettingInput!
    $condition: ModeldoctorsSettingConditionInput
  ) {
    deleteDoctorsSetting(input: $input, condition: $condition) {
      id
      roomName
      roomPasscode
      userName
      emailAlert
      smsAlert
      desktopAlert
      expDate
      cvv
      userId
      createdAt
      updatedAt
    }
  }
`;
export const createIllnesses = /* GraphQL */ `
  mutation CreateIllnesses(
    $input: CreateIllnessesInput!
    $condition: ModelillnessesConditionInput
  ) {
    createIllnesses(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const updateIllnesses = /* GraphQL */ `
  mutation UpdateIllnesses(
    $input: UpdateIllnessesInput!
    $condition: ModelillnessesConditionInput
  ) {
    updateIllnesses(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const deleteIllnesses = /* GraphQL */ `
  mutation DeleteIllnesses(
    $input: DeleteIllnessesInput!
    $condition: ModelillnessesConditionInput
  ) {
    deleteIllnesses(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const createInsurance = /* GraphQL */ `
  mutation CreateInsurance(
    $input: CreateInsuranceInput!
    $condition: ModelinsuranceConditionInput
  ) {
    createInsurance(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const updateInsurance = /* GraphQL */ `
  mutation UpdateInsurance(
    $input: UpdateInsuranceInput!
    $condition: ModelinsuranceConditionInput
  ) {
    updateInsurance(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const deleteInsurance = /* GraphQL */ `
  mutation DeleteInsurance(
    $input: DeleteInsuranceInput!
    $condition: ModelinsuranceConditionInput
  ) {
    deleteInsurance(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const createSpecialities = /* GraphQL */ `
  mutation CreateSpecialities(
    $input: CreateSpecialitiesInput!
    $condition: ModelspecialitiesConditionInput
  ) {
    createSpecialities(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const updateSpecialities = /* GraphQL */ `
  mutation UpdateSpecialities(
    $input: UpdateSpecialitiesInput!
    $condition: ModelspecialitiesConditionInput
  ) {
    updateSpecialities(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const deleteSpecialities = /* GraphQL */ `
  mutation DeleteSpecialities(
    $input: DeleteSpecialitiesInput!
    $condition: ModelspecialitiesConditionInput
  ) {
    deleteSpecialities(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
