/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getPatientProfile = /* GraphQL */ `
  query GetPatientProfile($id: ID!) {
    getPatientProfile(id: $id) {
      id
      firstName
      lastName
      gender
      homephoneNumber
      sma
      cellPhone
      language
      location
      preferredCommunication
      profile
      userId
      medicalHistoryconsent
      createdAt
      updatedAt
    }
  }
`;
export const listPatientProfiles = /* GraphQL */ `
  query ListPatientProfiles(
    $filter: ModelPatientProfileFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPatientProfiles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        firstName
        lastName
        gender
        homephoneNumber
        sma
        cellPhone
        language
        location
        preferredCommunication
        profile
        userId
        medicalHistoryconsent
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDoctorsProfile = /* GraphQL */ `
  query GetDoctorsProfile($id: ID!) {
    getDoctorsProfile(id: $id) {
      id
      firstName
      lastName
      speciality
      about
      inPerson
      inVideo
      title
      timeZone
      officePhone
      officeFax
      cellPhone
      practiceLogo
      preferedLanguage
      userId
      createdAt
      updatedAt
    }
  }
`;
export const listDoctorsProfiles = /* GraphQL */ `
  query ListDoctorsProfiles(
    $filter: ModeldoctorsProfileFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDoctorsProfiles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        firstName
        lastName
        speciality
        about
        inPerson
        inVideo
        title
        timeZone
        officePhone
        officeFax
        cellPhone
        practiceLogo
        preferedLanguage
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDoctorsCredentials = /* GraphQL */ `
  query GetDoctorsCredentials($id: ID!) {
    getDoctorsCredentials(id: $id) {
      id
      stateLicense
      deaNumber
      physicianDob
      prescriberName
      userId
      createdAt
      updatedAt
    }
  }
`;
export const listDoctorsCredentialss = /* GraphQL */ `
  query ListDoctorsCredentialss(
    $filter: ModeldoctorsCredentialsFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDoctorsCredentialss(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        stateLicense
        deaNumber
        physicianDob
        prescriberName
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDoctorsPractice = /* GraphQL */ `
  query GetDoctorsPractice($id: ID!) {
    getDoctorsPractice(id: $id) {
      id
      AddressLine1
      AddressLine2
      city
      state
      zip
      aboutPractice
      lattitude
      longitude
      userId
      createdAt
      updatedAt
    }
  }
`;
export const listDoctorsPractices = /* GraphQL */ `
  query ListDoctorsPractices(
    $filter: ModeldoctorsPracticeFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDoctorsPractices(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        AddressLine1
        AddressLine2
        city
        state
        zip
        aboutPractice
        lattitude
        longitude
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDoctorsEducation = /* GraphQL */ `
  query GetDoctorsEducation($id: ID!) {
    getDoctorsEducation(id: $id) {
      id
      education
      userId
      createdAt
      updatedAt
    }
  }
`;
export const listDoctorsEducations = /* GraphQL */ `
  query ListDoctorsEducations(
    $filter: ModeldoctorsEducationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDoctorsEducations(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        education
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDoctorsAcceptedInsurance = /* GraphQL */ `
  query GetDoctorsAcceptedInsurance($id: ID!) {
    getDoctorsAcceptedInsurance(id: $id) {
      id
      insurance
      userId
      createdAt
      updatedAt
    }
  }
`;
export const listDoctorsAcceptedInsurances = /* GraphQL */ `
  query ListDoctorsAcceptedInsurances(
    $filter: ModeldoctorsAcceptedInsuranceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDoctorsAcceptedInsurances(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        insurance
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDoctorsAffiliation = /* GraphQL */ `
  query GetDoctorsAffiliation($id: ID!) {
    getDoctorsAffiliation(id: $id) {
      id
      affiliation
      userId
      createdAt
      updatedAt
    }
  }
`;
export const listDoctorsAffiliations = /* GraphQL */ `
  query ListDoctorsAffiliations(
    $filter: ModeldoctorsAffiliationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDoctorsAffiliations(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        affiliation
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDoctorsSetting = /* GraphQL */ `
  query GetDoctorsSetting($id: ID!) {
    getDoctorsSetting(id: $id) {
      id
      roomName
      roomPasscode
      userName
      emailAlert
      smsAlert
      desktopAlert
      expDate
      cvv
      userId
      createdAt
      updatedAt
    }
  }
`;
export const listDoctorsSettings = /* GraphQL */ `
  query ListDoctorsSettings(
    $filter: ModeldoctorsSettingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDoctorsSettings(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        roomName
        roomPasscode
        userName
        emailAlert
        smsAlert
        desktopAlert
        expDate
        cvv
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getIllnesses = /* GraphQL */ `
  query GetIllnesses($name: String!) {
    getIllnesses(name: $name) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const listIllnessess = /* GraphQL */ `
  query ListIllnessess(
    $name: String
    $filter: ModelillnessesFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listIllnessess(
      name: $name
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        id
        name
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getInsurance = /* GraphQL */ `
  query GetInsurance($name: String!) {
    getInsurance(name: $name) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const listInsurances = /* GraphQL */ `
  query ListInsurances(
    $name: String
    $filter: ModelinsuranceFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listInsurances(
      name: $name
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        id
        name
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSpecialities = /* GraphQL */ `
  query GetSpecialities($name: String!) {
    getSpecialities(name: $name) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const listSpecialitiess = /* GraphQL */ `
  query ListSpecialitiess(
    $name: String
    $filter: ModelspecialitiesFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listSpecialitiess(
      name: $name
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        id
        name
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const doctorsProfileByUserid = /* GraphQL */ `
  query DoctorsProfileByUserid(
    $userId: String
    $sortDirection: ModelSortDirection
    $filter: ModeldoctorsProfileFilterInput
    $limit: Int
    $nextToken: String
  ) {
    doctorsProfileByUserid(
      userId: $userId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        firstName
        lastName
        speciality
        about
        inPerson
        inVideo
        title
        timeZone
        officePhone
        officeFax
        cellPhone
        practiceLogo
        preferedLanguage
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getdoctorsCredentialsByUserid = /* GraphQL */ `
  query GetdoctorsCredentialsByUserid(
    $userId: String
    $sortDirection: ModelSortDirection
    $filter: ModeldoctorsCredentialsFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getdoctorsCredentialsByUserid(
      userId: $userId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        stateLicense
        deaNumber
        physicianDob
        prescriberName
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getdoctorsPracticeByUserid = /* GraphQL */ `
  query GetdoctorsPracticeByUserid(
    $userId: String
    $sortDirection: ModelSortDirection
    $filter: ModeldoctorsPracticeFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getdoctorsPracticeByUserid(
      userId: $userId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        AddressLine1
        AddressLine2
        city
        state
        zip
        aboutPractice
        lattitude
        longitude
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getdoctorsEducationByUserid = /* GraphQL */ `
  query GetdoctorsEducationByUserid(
    $userId: String
    $sortDirection: ModelSortDirection
    $filter: ModeldoctorsEducationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getdoctorsEducationByUserid(
      userId: $userId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        education
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getdoctorsAcceptedInsuranceByUserid = /* GraphQL */ `
  query GetdoctorsAcceptedInsuranceByUserid(
    $userId: String
    $sortDirection: ModelSortDirection
    $filter: ModeldoctorsAcceptedInsuranceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getdoctorsAcceptedInsuranceByUserid(
      userId: $userId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        insurance
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getdoctorsAffiliationByUserid = /* GraphQL */ `
  query GetdoctorsAffiliationByUserid(
    $userId: String
    $sortDirection: ModelSortDirection
    $filter: ModeldoctorsAffiliationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getdoctorsAffiliationByUserid(
      userId: $userId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        affiliation
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getdoctorsSettingByUserid = /* GraphQL */ `
  query GetdoctorsSettingByUserid(
    $userId: String
    $sortDirection: ModelSortDirection
    $filter: ModeldoctorsSettingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getdoctorsSettingByUserid(
      userId: $userId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        roomName
        roomPasscode
        userName
        emailAlert
        smsAlert
        desktopAlert
        expDate
        cvv
        userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
