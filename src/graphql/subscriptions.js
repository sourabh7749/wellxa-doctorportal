/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreatePatientProfile = /* GraphQL */ `
  subscription OnCreatePatientProfile {
    onCreatePatientProfile {
      id
      firstName
      lastName
      gender
      homephoneNumber
      sma
      cellPhone
      language
      location
      preferredCommunication
      profile
      userId
      medicalHistoryconsent
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePatientProfile = /* GraphQL */ `
  subscription OnUpdatePatientProfile {
    onUpdatePatientProfile {
      id
      firstName
      lastName
      gender
      homephoneNumber
      sma
      cellPhone
      language
      location
      preferredCommunication
      profile
      userId
      medicalHistoryconsent
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePatientProfile = /* GraphQL */ `
  subscription OnDeletePatientProfile {
    onDeletePatientProfile {
      id
      firstName
      lastName
      gender
      homephoneNumber
      sma
      cellPhone
      language
      location
      preferredCommunication
      profile
      userId
      medicalHistoryconsent
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDoctorsProfile = /* GraphQL */ `
  subscription OnCreateDoctorsProfile {
    onCreateDoctorsProfile {
      id
      firstName
      lastName
      speciality
      title
      timeZone
      officePhone
      officeFax
      cellPhone
      practiceLogo
      preferedLanguage
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDoctorsProfile = /* GraphQL */ `
  subscription OnUpdateDoctorsProfile {
    onUpdateDoctorsProfile {
      id
      firstName
      lastName
      speciality
      title
      timeZone
      officePhone
      officeFax
      cellPhone
      practiceLogo
      preferedLanguage
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDoctorsProfile = /* GraphQL */ `
  subscription OnDeleteDoctorsProfile {
    onDeleteDoctorsProfile {
      id
      firstName
      lastName
      speciality
      title
      timeZone
      officePhone
      officeFax
      cellPhone
      practiceLogo
      preferedLanguage
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDoctorsCredentials = /* GraphQL */ `
  subscription OnCreateDoctorsCredentials {
    onCreateDoctorsCredentials {
      id
      stateLicense
      deaNumber
      physicianDob
      prescriberName
      education1
      education2
      education3
      affiliation1
      affiliation2
      affiliation3
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDoctorsCredentials = /* GraphQL */ `
  subscription OnUpdateDoctorsCredentials {
    onUpdateDoctorsCredentials {
      id
      stateLicense
      deaNumber
      physicianDob
      prescriberName
      education1
      education2
      education3
      affiliation1
      affiliation2
      affiliation3
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDoctorsCredentials = /* GraphQL */ `
  subscription OnDeleteDoctorsCredentials {
    onDeleteDoctorsCredentials {
      id
      stateLicense
      deaNumber
      physicianDob
      prescriberName
      education1
      education2
      education3
      affiliation1
      affiliation2
      affiliation3
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDoctorsPractice = /* GraphQL */ `
  subscription OnCreateDoctorsPractice {
    onCreateDoctorsPractice {
      id
      AddressLine1
      AddressLine2
      city
      state
      zip
      aboutPractice
      education3
      affiliation1
      affiliation2
      affiliation3
      lattitude
      longitude
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDoctorsPractice = /* GraphQL */ `
  subscription OnUpdateDoctorsPractice {
    onUpdateDoctorsPractice {
      id
      AddressLine1
      AddressLine2
      city
      state
      zip
      aboutPractice
      education3
      affiliation1
      affiliation2
      affiliation3
      lattitude
      longitude
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDoctorsPractice = /* GraphQL */ `
  subscription OnDeleteDoctorsPractice {
    onDeleteDoctorsPractice {
      id
      AddressLine1
      AddressLine2
      city
      state
      zip
      aboutPractice
      education3
      affiliation1
      affiliation2
      affiliation3
      lattitude
      longitude
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDoctorsSetting = /* GraphQL */ `
  subscription OnCreateDoctorsSetting {
    onCreateDoctorsSetting {
      id
      roomName
      roomPasscode
      userName
      emailAlert
      smsAlert
      desktopAlert
      expDate
      cvv
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDoctorsSetting = /* GraphQL */ `
  subscription OnUpdateDoctorsSetting {
    onUpdateDoctorsSetting {
      id
      roomName
      roomPasscode
      userName
      emailAlert
      smsAlert
      desktopAlert
      expDate
      cvv
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDoctorsSetting = /* GraphQL */ `
  subscription OnDeleteDoctorsSetting {
    onDeleteDoctorsSetting {
      id
      roomName
      roomPasscode
      userName
      emailAlert
      smsAlert
      desktopAlert
      expDate
      cvv
      userId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateIllnesses = /* GraphQL */ `
  subscription OnCreateIllnesses {
    onCreateIllnesses {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateIllnesses = /* GraphQL */ `
  subscription OnUpdateIllnesses {
    onUpdateIllnesses {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteIllnesses = /* GraphQL */ `
  subscription OnDeleteIllnesses {
    onDeleteIllnesses {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSpecialities = /* GraphQL */ `
  subscription OnCreateSpecialities {
    onCreateSpecialities {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSpecialities = /* GraphQL */ `
  subscription OnUpdateSpecialities {
    onUpdateSpecialities {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSpecialities = /* GraphQL */ `
  subscription OnDeleteSpecialities {
    onDeleteSpecialities {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
