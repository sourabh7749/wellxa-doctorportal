import React from "react";
import {
  // MixedWidget4,
  BaseTablesWidget1,
  BaseTablesWidget2,
  // BaseTablesWidget6,
  // StatsWidget11,
  // StatsWidget10,
  // ListsWidget8,
  // ListsWidget10,
  // ListsWidget14,
  AdvanceTablesWidget9,
} from "../widgets";

export function Demo3Dashboard() {
  return (
    <>
      {/* begin::Dashboard */}

      {/* begin::Row */}
      <div className="row">
        <div className="col-lg-12 col-xxl-12">
          <AdvanceTablesWidget9 className="card-stretch gutter-b" />
        </div>
      </div>
      {/* end::Row */}

      {/* begin::Row */}
      <div className="row">
        <div className="col-lg-5">
        <BaseTablesWidget1 className="card-stretch gutter-b" />
        </div>
        <div className="col-lg-7">
          <BaseTablesWidget2 className="card-stretch gutter-b" />
        </div>
      </div>
      {/* end::Row */}

      {/* end::Dashboard */}
    </>
  );
}
