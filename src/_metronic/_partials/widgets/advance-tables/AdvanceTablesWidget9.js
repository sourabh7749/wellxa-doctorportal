/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../_helpers";
// import { Icon } from "@material-ui/core";

export function AdvanceTablesWidget9({ className }) {
  return (
    <>
      {/* begin::Advance Table Widget 9 */}
      <div className={`card card-custom ${className}`}>
        {/* begin::Header */}
        <div className="card-header border-0 py-5">
          <h3 className="card-title align-items-start flex-column">
            <span className="card-label font-weight-bolder text-dark">
            Waiting Room 
            </span>
            <span className="text-muted mt-3 font-weight-bold font-size-sm">
              More than 400+ new members
            </span>
          </h3>
          <div className="card-toolbar">
            <a
              href="#"
              className="btn btn-info font-weight-bolder font-size-sm mr-3"
            >
              +
            </a>
          </div>
        </div>
        {/* end::Header */}

        {/* begin::Body */}
        <div className="card-body pt-0 pb-3">
          <div className="tab-content">
            {/* begin::Table */}
            <div className="table-responsive">
              <table className="table table-head-custom table-vertical-center table-head-bg table-borderless">
                <thead>
                  <tr className="text-left">
                    <th  className="pl-7">
                      <span className="text-dark-75">Schedule Time</span>
                    </th>
                    <th >Patient First Name</th>
                    <th >Patient Last Name</th>
                    <th >Reason</th>
                    <th >DOB</th>
                    <th>Address </th>
                    <th>Type of Visit </th>
                    <th>Pharmacy </th>
                    <th>Notes </th>
                    <th>Action </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="pl-0 py-8">
                      <div className="d-flex align-items-center">
                        <div className="symbol symbol-50 symbol-light mr-4">
                          <span className="symbol-label">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/avatars/001-boy.svg"
                              )}
                            ></SVG>
                          </span>
                        </div>
                        <div>
                          <a
                            href="#"
                            className="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg"
                          >
                            10:30 am
                          </a>
                        </div>
                      </div>
                    </td>
                    <td>
                        Sunny
                    </td>
                    <td>
                      Naik
                    </td>
                    <td>
                      Fever
                    </td>
                    <td>
                      16/02/1994
                    </td>
                    <td className="pr-0 text-right">
                    1 Grace Ct
                    </td>
                    <td className="pr-0 text-right">
                      Video
                    </td>
                    <td className="pr-0 text-right">
                    Plainsoboro
                    </td>
                    <td className="pr-0 text-right">
                    Emergency
                    </td>
                    <td className="pr-0 text-right">
                    Icons
                    </td>
                  </tr>
                  <tr>
                    <td className="pl-0 py-8">
                      <div className="d-flex align-items-center">
                        <div className="symbol symbol-50 symbol-light mr-4">
                          <span className="symbol-label">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/avatars/001-boy.svg"
                              )}
                            ></SVG>
                          </span>
                        </div>
                        <div>
                          <a
                            href="#"
                            className="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg"
                          >
                            10:30 am
                          </a>
                        </div>
                      </div>
                    </td>
                    <td>
                        Sunny
                    </td>
                    <td>
                      Naik
                    </td>
                    <td>
                      Fever
                    </td>
                    <td>
                      16/02/1994
                    </td>
                    <td className="pr-0 text-right">
                    1 Grace Ct
                    </td>
                    <td className="pr-0 text-right">
                      Video
                    </td>
                    <td className="pr-0 text-right">
                    Plainsoboro
                    </td>
                    <td className="pr-0 text-right">
                    Emergency
                    </td>
                    <td className="pr-0 text-right">
                    Icons
                    </td>
                  </tr>
                  <tr>
                    <td className="pl-0 py-8">
                      <div className="d-flex align-items-center">
                        <div className="symbol symbol-50 symbol-light mr-4">
                          <span className="symbol-label">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/avatars/001-boy.svg"
                              )}
                            ></SVG>
                          </span>
                        </div>
                        <div>
                          <a
                            href="#"
                            className="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg"
                          >
                            10:30 am
                          </a>
                        </div>
                      </div>
                    </td>
                    <td>
                        Sunny
                    </td>
                    <td>
                      Naik
                    </td>
                    <td>
                      Fever
                    </td>
                    <td>
                      16/02/1994
                    </td>
                    <td className="pr-0 text-right">
                    1 Grace Ct
                    </td>
                    <td className="pr-0 text-right">
                      Video
                    </td>
                    <td className="pr-0 text-right">
                    Plainsoboro
                    </td>
                    <td className="pr-0 text-right">
                    Emergency
                    </td>
                    <td className="pr-0 text-right">
                    Icons
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            {/* end::Table */}
          </div>
        </div>
        {/* end::Body */}
      </div>
      {/* end::Advance Table Widget 9 */}
    </>
  );
}
