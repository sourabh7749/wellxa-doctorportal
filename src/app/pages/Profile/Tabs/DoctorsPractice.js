// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { Formik, Form, Field } from "formik";
import { Input } from "../../../../_metronic/_partials/controls";
import Geocode from "react-geocode";
// Validation schema

export function DoctorsPractice({ product, btnRef, saveProduct }) {

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={product}
        onSubmit={(values) => {
          saveProduct(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="AddressLine1"
                    component={Input}
                    placeholder="AddressLine1"
                    label="AddressLine1"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="AddressLine2"
                    component={Input}
                    placeholder="AddressLine2"
                    label="AddressLine2"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="city"
                    component={Input}
                    placeholder="City"
                    label="City"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="state"
                    component={Input}
                    placeholder="State"
                    label="State"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="zip"
                    component={Input}
                    placeholder="Zip"
                    label="Zip"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="aboutPractice"
                    component={Input}
                    placeholder="About Practice"
                    label="About Practice"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="education3"
                    component={Input}
                    placeholder="Education3"
                    label="Education3"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="affiliation1"
                    component={Input}
                    placeholder="Affiliation1"
                    label="Affiliation1"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="affiliation2"
                    component={Input}
                    placeholder="Affiliation2"
                    label="Affiliation2"
                  />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="affiliation3"
                    component={Input}
                    placeholder="Affiliation 3"
                    label="Affiliation 3"
                  />
                </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
