// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { Formik, Form, Field  } from "formik";
import { Input } from "../../../../_metronic/_partials/controls";
// Validation schema

export function Credentials({
  product,
  btnRef,
  saveProduct,
}) {
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={product}
        onSubmit={(values) => {
          saveProduct(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
                        <div className="form-group row">
              <div className="col-lg-4">
                <Field
                  name="stateLicense"
                  component={Input}
                  placeholder="State Licence"
                  label="State Licence"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="deaNumber"
                  component={Input}
                  placeholder="Dea Number"
                  label="Dea Number"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="physicianDob"
                  component={Input}
                  placeholder="Physician Dob"
                  label="Physician Dob"
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-lg-4">
                <Field
                  name="prescriberName"
                  component={Input}
                  placeholder="Prescriber Name"
                  label="Prescriber Name"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="education1"
                  component={Input}
                  placeholder="Education1"
                  label="Education1"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="education2"
                  component={Input}
                  placeholder="Education2"
                  label="education2"
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-lg-4">
                <Field
                  name="education3"
                  component={Input}
                  placeholder="Education3"
                  label="Education3"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="affiliation1"
                  component={Input}
                  placeholder="Affiliation1"
                  label="Affiliation1"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="affiliation2"
                  component={Input}
                  placeholder="Affiliation2"
                  label="Affiliation2"
                />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-lg-4">
                <Field
                  name="affiliation3"
                  component={Input}
                  placeholder="Affiliation 3"
                  label="Affiliation 3"
                />
              </div>
            </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
