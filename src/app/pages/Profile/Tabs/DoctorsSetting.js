// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { Formik, Form, Field } from "formik";
import { Input } from "../../../../_metronic/_partials/controls";
// Validation schema

export function DoctorsSetting({ product, btnRef, saveProduct }) {
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={product}
        onSubmit={(values) => {
          saveProduct(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="roomName"
                    component={Input}
                    placeholder="Room Name"
                    label="Room Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="roomPasscode"
                    component={Input}
                    placeholder="Room Passcode"
                    label="Room Passcode"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="userName"
                    component={Input}
                    placeholder="User Name"
                    label="User Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="emailAlert"
                    component={Input}
                    placeholder="Email Alert"
                    label="Email Alert"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="smsAlert"
                    component={Input}
                    placeholder="Sms Alert"
                    label="Sms Alert"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="desktopAlert"
                    component={Input}
                    placeholder="Desktop Alert"
                    label="Desktop Alert"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="expDate"
                    component={Input}
                    placeholder="Exp Date"
                    label="Exp Date"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="cvv"
                    component={Input}
                    placeholder="cvv"
                    label="cvv"
                  />
                </div>
              </div>

              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
