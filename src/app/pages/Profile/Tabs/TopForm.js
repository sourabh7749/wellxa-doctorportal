// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { Formik, Form, Field  } from "formik";
import { Input } from "../../../../_metronic/_partials/controls";
// Validation schema

export function TopForm({
  product,
  btnRef,
  saveProduct,
}) {
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={product}
        onSubmit={(values) => {
          saveProduct(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
                        <div className="form-group row">
              <div className="col-lg-4">
                <Field
                  name="firstName"
                  component={Input}
                  placeholder="First Name"
                  label="First Name"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="lastName"
                  component={Input}
                  placeholder="Last Name"
                  label="Last Name"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="speciality"
                  component={Input}
                  placeholder="Speciality"
                  label="Speciality"
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-lg-4">
                <Field
                  name="title"
                  component={Input}
                  placeholder="Title"
                  label="Title"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="timeZone"
                  component={Input}
                  placeholder="Timezone"
                  label="Timezone"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="officePhone"
                  component={Input}
                  placeholder="Office Phone"
                  label="Office Phone"
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-lg-4">
                <Field
                  name="officeFax"
                  component={Input}
                  placeholder="Office Fax"
                  label="Office Fax"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="cellPhone"
                  component={Input}
                  placeholder="Cell Phone"
                  label="Cell Phone"
                />
              </div>
              <div className="col-lg-4">
                <Field
                  name="practiceLogo"
                  component={Input}
                  placeholder="practiceLogo"
                  label="practiceLogo"
                />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-lg-4">
                <Field
                  name="preferedLanguage"
                  component={Input}
                  placeholder="Prefered Language"
                  label="Prefered Language"
                />
              </div>
            </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
