/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { shallowEqual, useSelector } from "react-redux";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
import { TopForm } from "./Tabs/TopForm";
import { Credentials } from "./Tabs/Credentials";
import { DoctorsPractice } from "./Tabs/DoctorsPractice";
import { DoctorsSetting } from "./Tabs/DoctorsSetting";
import { useSubheader } from "../../../_metronic/layout";
import { ModalProgressBar } from "../../../_metronic/_partials/controls";

import {
  updateDoctorsProfile,
  updateDoctorsCredentials,
  updateDoctorsPractice,
  updateDoctorsSetting,
} from "../../../graphql/mutations";
import {
  getdoctorsCredentialsByUserid,
  getdoctorsPracticeByUserid,
  doctorsProfileByUserid,
  getdoctorsSettingByUserid,
} from "../../../graphql/queries";
import { API, graphqlOperation } from "aws-amplify";
import Geocode from "react-geocode";
Geocode.setApiKey("AIzaSyDNg7Nl1wvGsj-XZjYftT_DuGUAg7bTB6w");

Geocode.setLanguage("en");

Geocode.setRegion("es");

Geocode.enableDebug();

export function ProductEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const [DoctorsCredentialsState, setDoctorsCredentials] = useState({});
  const [DoctorsPracticeState, setDoctorsPractice] = useState({});
  const [DoctorsProfileState, setDoctorsProfile] = useState({});
  const [DoctorsSettingState, setDoctorsSetting] = useState({});
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, productForEdit } = useSelector(
    (state) => ({}),
    shallowEqual
  );

  useEffect(() => {
    getDoctorsProfileData();
    getDoctorsPracticeData();
    getDoctorsCredentialsData();
    getDoctorsSettingsData();
  }, [tab]);

  const getDoctorsProfileData = async () => {
    let variables = {};
    variables.userId = localStorage.getItem("userId");
    let datas = await API.graphql(
      graphqlOperation(doctorsProfileByUserid, variables)
    );
    let doctorsInfo =
      datas.data.doctorsProfileByUserid.items &&
      datas.data.doctorsProfileByUserid.items[0];
    setDoctorsProfile(doctorsInfo);
  };
  const getDoctorsPracticeData = async () => {
    let variables = {};
    variables.userId = localStorage.getItem("userId");
    let datas = await API.graphql(
      graphqlOperation(getdoctorsPracticeByUserid, variables)
    );
    let doctorsInfo =
      datas.data.getdoctorsPracticeByUserid.items &&
      datas.data.getdoctorsPracticeByUserid.items[0];
    setDoctorsPractice(doctorsInfo);
  };
  const getDoctorsCredentialsData = async () => {
    let variables = {};
    variables.userId = localStorage.getItem("userId");
    let datas = await API.graphql(
      graphqlOperation(getdoctorsCredentialsByUserid, variables)
    );
    let doctorsInfo =
      datas.data.getdoctorsCredentialsByUserid.items &&
      datas.data.getdoctorsCredentialsByUserid.items[0];
    setDoctorsCredentials(doctorsInfo);
  };
  const getDoctorsSettingsData = async () => {
    let variables = {};
    variables.userId = localStorage.getItem("userId");
    let datas = await API.graphql(
      graphqlOperation(getdoctorsSettingByUserid, variables)
    );
    let doctorsInfo =
      datas.data.getdoctorsSettingByUserid.items &&
      datas.data.getdoctorsSettingByUserid.items[0];
    setDoctorsSetting(doctorsInfo);
  };

  useEffect(() => {
    let _title = "Doctors Profile";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productForEdit, id]);

  const saveProduct = (values) => {
    delete values.createdAt;
    delete values.updatedAt;
    delete values.__typename;
    var variables = {};
    variables.input = values;
    if (tab === "practice") {
      Geocode.fromAddress(
        values.AddressLine1
      ).then(
        (response) => {
          const { lat, lng } = response.results[0].geometry.location;
          values.lattitude =   JSON.stringify(lat);
          values.longitude = JSON.stringify(lng);
          variables.input = values;
          console.log("variables")
          console.log(variables)
        },
        (error) => {
          console.error(error);
        }
      );
      let data = API.graphql(
        graphqlOperation(updateDoctorsPractice, variables)
      );
      if (!data.messages) {
        alert("Updated Successfully");
      }
    }

    if (tab === "basic") {
      let data = API.graphql(graphqlOperation(updateDoctorsProfile, variables));
      if (!data.messages) {
        alert("Updated Successfully");
      }
    }
    if (tab === "credentials") {
      let data = API.graphql(
        graphqlOperation(updateDoctorsCredentials, variables)
      );
      if (!data.messages) {
        alert("Updated Successfully");
      }
    }

    if (tab === "setting") {
      let data = API.graphql(graphqlOperation(updateDoctorsSetting, variables));
      if (!data.messages) {
        alert("Updated Successfully");
      }
    }
  };

  const btnRef = useRef();
  const saveProductClick = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backToProductsList = () => {
    history.push(`/e-commerce/products`);
  };

  return (
    <Card>
      {actionsLoading && <ModalProgressBar />}
      <CardHeader title={title}>
        <CardHeaderToolbar>
          <button
            type="button"
            onClick={backToProductsList}
            className="btn btn-light"
          >
            <i className="fa fa-arrow-left"></i>
            Back
          </button>
          {`  `}
          <button className="btn btn-light ml-2">
            <i className="fa fa-redo"></i>
            Reset
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-primary ml-2"
            onClick={saveProductClick}
          >
            Save
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ul className="nav nav-tabs nav-tabs-line " role="tablist">
          <li className="nav-item" onClick={() => setTab("basic")}>
            <a
              className={`nav-link ${tab === "basic" && "active"}`}
              data-toggle="tab"
              role="tab"
              aria-selected={(tab === "basic").toString()}
            >
              Basic info
            </a>
          </li>
          <li className="nav-item" onClick={() => setTab("credentials")}>
            <a
              className={`nav-link ${tab === "credentials" && "active"}`}
              data-toggle="tab"
              role="tab"
              aria-selected={(tab === "credentials").toString()}
            >
              Credentials
            </a>
          </li>
          <li className="nav-item" onClick={() => setTab("practice")}>
            <a
              className={`nav-link ${tab === "practice" && "active"}`}
              data-toggle="tab"
              role="tab"
              aria-selected={(tab === "practice").toString()}
            >
              Practice
            </a>
          </li>
          <li className="nav-item" onClick={() => setTab("setting")}>
            <a
              className={`nav-link ${tab === "setting" && "active"}`}
              data-toggle="tab"
              role="tab"
              aria-selected={(tab === "setting").toString()}
            >
              Settings
            </a>
          </li>
        </ul>
        <div className="mt-5">
          {tab === "basic" && (
            <TopForm
              actionsLoading={actionsLoading}
              product={DoctorsProfileState}
              btnRef={btnRef}
              saveProduct={saveProduct}
            />
          )}
          {tab === "credentials" && (
            <Credentials
              actionsLoading={actionsLoading}
              product={DoctorsCredentialsState}
              btnRef={btnRef}
              saveProduct={saveProduct}
            />
          )}
          {tab === "practice" && (
            <DoctorsPractice
              actionsLoading={actionsLoading}
              product={DoctorsPracticeState}
              btnRef={btnRef}
              saveProduct={saveProduct}
            />
          )}
          {tab === "setting" && (
            <DoctorsSetting
              actionsLoading={actionsLoading}
              product={DoctorsSettingState}
              btnRef={btnRef}
              saveProduct={saveProduct}
            />
          )}
        </div>
      </CardBody>
    </Card>
  );
}
