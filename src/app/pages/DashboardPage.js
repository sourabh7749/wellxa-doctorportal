import React from "react";
import {
  Dashboard
} from "../../_metronic/_partials";
import { useQuery } from '@apollo/react-hooks';
import gql from "graphql-tag";



const GET_POKEMON_INFO = gql`
query GetDoctorsProfile {
  getDoctorsProfile(id: "a2d967b5-6d82-4e3d-a71a-645aec8648f4") {
    id
    firstName
    lastName
    speciality
    title
    timeZone
    officePhone
    officeFax
    cellPhone
    practiceLogo
    preferedLanguage
    userId
    createdAt
    updatedAt
  }
}
`
export function DashboardPage() {
  const { data, loading, error } = useQuery(GET_POKEMON_INFO);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error</p>;
  

  localStorage.setItem('Doctorsdata', JSON.stringify(data.getDoctorsProfile));
  
  return <Dashboard />;
}
